# Mensa Uniurb News Bot.py #

Semplice Bot Telegram per la visualizzazione delle news della mensa offerta dall'ERSU di Urbino.

Se il bot vi è stato utile, offrimi una birra:

[![gitcheese.com](https://s3.amazonaws.com/gitcheese-ui-master/images/badge.svg)](https://www.gitcheese.com/donate/users/5260133/repos/62651199)