# Your Bot token (get it from BotFather)
token = 'TOKEN'

# Message send when user 'start' the bot
start_msg = "Benvenuto su @MensaUniurbNews_Bot\n"\
            "Semplice Bot Telegram per la visualizzazione delle news\n"\
            "della mensa offerta dall'ERSU di Urbino.\n"\

# Pool frequence
refresh_time = 5 * 60

# Message send when user 'start' the bot
stop_msg = "*Notifiche disattivate*"

# File with 'started' clients
client_file = "clients_id.txt"